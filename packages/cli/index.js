import React, { useState, Fragment } from "react";
import { render, Box, Color } from "ink";
import union from "folktale/adt/union/union";

import Table from "ink-table";
import TextInput from "ink-text-input";
import Divider from "ink-divider";
import Spinner from "ink-spinner";

import { getEvolutions } from "next-order-evolutions-source";

const capitalize = (s = "") => s.charAt(0).toUpperCase() + s.slice(1);

const Evolutions = union("Evolutions", {
  None: () => {},
  Loading: () => {},
  Some: (evolutions) => ({ evolutions }),
  NotAsked: () => {},
  Error: (error) => ({ error }),
});

const CLI = () => {
  const [query, setQuery] = useState("");
  const [lastQuery, setLastQuery] = useState("");
  const [evolutions, setEvolutions] = useState(Evolutions.NotAsked());

  const onSearch = async (name) => {
    setEvolutions(Evolutions.Loading());
    try {
      // Make name capitalized since the api requires it
      const cleanedName = capitalize(name).trim();
      const evolutions = await getEvolutions(cleanedName);
      setLastQuery(cleanedName);
      setQuery("");
      if (evolutions.length === 0) {
        setEvolutions(Evolutions.None());
      } else {
        setEvolutions(Evolutions.Some(evolutions));
      }
    } catch (e) {
      setEvolutions(Evolutions.Error(e));
    }
  };
  /* eslint-disable react/display-name, react/prop-types */
  return (
    <span>
      <Box>
        <Box marginRight={1}>Digimon Name:</Box>
        <TextInput value={query} onChange={setQuery} onSubmit={onSearch} />
      </Box>
      <br />
      <Divider />
      {evolutions.matchWith({
        NotAsked: () => (
          <Box>
            Welcome to next-order-evolutions, enter a Digimon&apos;s name to see
            its tree
          </Box>
        ),
        Loading: () => (
          <Fragment>
            <Color green>
              <Spinner type="dots" />
            </Color>
            {" Loading"}
          </Fragment>
        ),
        Some: ({ evolutions }) => (
          <span>
            <Color green>{lastQuery}</Color>
            <br />
            <Table
              data={evolutions.map((evo) => ({
                name: evo.name,
                ...evo.requirements,
              }))}
            />
          </span>
        ),
        None: () => <Box>Digimon not found</Box>,
        Error: () => <Box>Something went wrong</Box>,
      })}
    </span>
  );
};

render(<CLI />);
