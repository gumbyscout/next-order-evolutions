port module Ports exposing (..)

import Json.Decode exposing (Value)


port getEvolutionTree : String -> Cmd msg


port gotEvolutionTree : (Value -> msg) -> Sub msg


port digimonNotFound : (String -> msg) -> Sub msg
