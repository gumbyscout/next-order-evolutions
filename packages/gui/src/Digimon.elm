module Digimon exposing (..)

import Dict
import Html exposing (..)
import Html.Attributes as Attr
import Html.Lazy exposing (lazy)
import Json.Decode as Decode exposing (Decoder, dict, int, list, map, maybe, string)
import Json.Decode.Pipeline exposing (optional, required)
import List


type alias Requirements =
    { hp : Maybe String
    , mp : Maybe String
    , strength : Maybe String
    , stamina : Maybe String
    , wisdom : Maybe String
    , speed : Maybe String
    , weight : Maybe String
    , mistakes : Maybe String
    , bond : Maybe String
    , discipline : Maybe String
    , wins : Maybe String
    , keyDigimon : Maybe String
    , keyPoints : Maybe String
    }


type Stage
    = Baby
    | InTraining
    | Rookie
    | Champion
    | Ultimate
    | Mega


type alias Digimon =
    { name : String
    , stage : Stage
    , requirements : Maybe Requirements
    , evolutions : Maybe Evolutions
    }



-- We need this second type to get around recursion issues with decoders


type Evolutions
    = Evolutions (List Digimon)


stageToString : Stage -> String
stageToString stage =
    case stage of
        Baby ->
            "Baby"

        InTraining ->
            "In-Training"

        Rookie ->
            "Rookie"

        Champion ->
            "Champion"

        Ultimate ->
            "Ultimate"

        Mega ->
            "Mega"


stageFromString : String -> Decoder Stage
stageFromString stage =
    case stage of
        "baby" ->
            Decode.succeed Baby

        "inTraining" ->
            Decode.succeed InTraining

        "rookie" ->
            Decode.succeed Rookie

        "champion" ->
            Decode.succeed Champion

        "ultimate" ->
            Decode.succeed Ultimate

        "mega" ->
            Decode.succeed Mega

        _ ->
            Decode.fail "unknown stage"


emptyJsonDecoder : Decoder (Maybe a)
emptyJsonDecoder =
    dict int
        |> Decode.andThen
            (\entries ->
                case Dict.size entries of
                    0 ->
                        Decode.succeed Nothing

                    _ ->
                        Decode.fail "Expected empty JSON object"
            )


requirementsDecoder : Decoder Requirements
requirementsDecoder =
    Decode.succeed Requirements
        |> optional "hp" (maybe string) Maybe.Nothing
        |> optional "mp" (maybe string) Maybe.Nothing
        |> optional "strength" (maybe string) Maybe.Nothing
        |> optional "stamina" (maybe string) Maybe.Nothing
        |> optional "wisdom" (maybe string) Maybe.Nothing
        |> optional "speed" (maybe string) Maybe.Nothing
        |> optional "weight" (maybe string) Maybe.Nothing
        |> optional "mistakes" (maybe string) Maybe.Nothing
        |> optional "bond" (maybe string) Maybe.Nothing
        |> optional "discipline" (maybe string) Maybe.Nothing
        |> optional "wins" (maybe string) Maybe.Nothing
        |> optional "keyDigimon" (maybe string) Maybe.Nothing
        |> optional "keyPoints" (maybe string) Maybe.Nothing



-- This second decoder is also to get around recursion, as well as the lazy statements


evolutionsDecoder : Decoder Evolutions
evolutionsDecoder =
    Decode.map Evolutions <| list (Decode.lazy (\_ -> digimonDecoder))


stageDecoder : Decoder Stage
stageDecoder =
    string
        |> Decode.andThen stageFromString


digimonDecoder : Decoder Digimon
digimonDecoder =
    Decode.succeed Digimon
        |> required "name" string
        |> required "stage" stageDecoder
        |> optional "requirements" (Decode.oneOf [ emptyJsonDecoder, maybe requirementsDecoder ]) Nothing
        |> optional "evolutions" (Decode.lazy (\_ -> maybe evolutionsDecoder)) Nothing


evolutionsView : Maybe Evolutions -> Bool -> Html msg
evolutionsView evos open =
    case evos of
        Just (Evolutions evolutions) ->
            details
                (if open then
                    [ Attr.attribute "open" "open" ]

                 else
                    []
                )
                [ summary
                    []
                    [ text "Evolutions"
                    ]
                , div
                    [ Attr.class "evolution-node__evolutions" ]
                  <|
                    List.map
                        (\evolution -> lazy (\e -> nodeView e) evolution)
                        evolutions
                ]

        Nothing ->
            text ""


requirementsView : Maybe Requirements -> Html msg
requirementsView requirements =
    case requirements of
        Nothing ->
            text ""

        Just r ->
            details []
                [ summary [] [ text "Requirements" ]
                , table [ Attr.class "table" ]
                    [ thead []
                        [ tr []
                            [ th [] [ text "HP" ]
                            , th [] [ text "MP" ]
                            , th [] [ text "Strength" ]
                            , th [] [ text "Stamina" ]
                            , th [] [ text "Wisdom" ]
                            , th [] [ text "Speed" ]
                            , th [] [ text "Weight" ]
                            ]
                        ]
                    , tbody []
                        [ tr []
                            [ td [] [ text <| Maybe.withDefault "-" r.hp ]
                            , td [] [ text <| Maybe.withDefault "-" r.mp ]
                            , td [] [ text <| Maybe.withDefault "-" r.strength ]
                            , td [] [ text <| Maybe.withDefault "-" r.stamina ]
                            , td [] [ text <| Maybe.withDefault "-" r.wisdom ]
                            , td [] [ text <| Maybe.withDefault "-" r.speed ]
                            , td [] [ text <| Maybe.withDefault "-" r.weight ]
                            ]
                        ]
                    , thead []
                        [ tr []
                            [ th [] [ text "Mistakes" ]
                            , th [] [ text "Bond" ]
                            , th [] [ text "Discipline" ]
                            , th [] [ text "Wins" ]
                            , th [] [ text "Key Digimon" ]
                            , th [ Attr.colspan 2 ] [ text "Key Points" ]
                            ]
                        ]
                    , tbody []
                        [ tr []
                            [ td [] [ text <| Maybe.withDefault "-" r.mistakes ]
                            , td [] [ text <| Maybe.withDefault "-" r.bond ]
                            , td [] [ text <| Maybe.withDefault "-" r.discipline ]
                            , td [] [ text <| Maybe.withDefault "-" r.wins ]
                            , td [] [ text <| Maybe.withDefault "-" r.keyDigimon ]
                            , td [ Attr.colspan 2 ] [ text <| Maybe.withDefault "-" r.keyPoints ]
                            ]
                        ]
                    ]
                ]


wikiURL : Digimon -> String
wikiURL digimon =
    "https://wikimon.net/" ++ digimon.name


iconPath : String -> String
iconPath name =
    String.replace "(" "" name
        |> String.replace ")" ""
        |> String.replace " " "-"
        |> String.toLower
        |> (\str -> "/images/" ++ str ++ "-icon.png")


nodeView : Digimon -> Html msg
nodeView digimon =
    div [ Attr.class "tile evolution-node" ]
        [ div [ Attr.class "tile-icon" ]
            [ img
                [ Attr.class "digimon-icon", Attr.src <| iconPath digimon.name, Attr.alt "" ]
                []
            ]
        , section [ Attr.class "tile-content" ]
            [ header []
                [ p [ Attr.class "tile-title h5" ] [ text digimon.name ]
                , p [ Attr.class "tile-subtitle h6" ] [ text <| stageToString digimon.stage ]
                , a [ Attr.class "tile-subtitle h6", Attr.href <| wikiURL digimon ] [ text "Wikimon page" ]
                ]
            , requirementsView digimon.requirements
            , evolutionsView digimon.evolutions False
            ]
        ]


view : Digimon -> Html msg
view digimon =
    div []
        [ img [ Attr.class "digimon-icon", Attr.src <| iconPath digimon.name, Attr.alt "" ] []
        , h2 []
            [ text digimon.name
            , p [ Attr.class "h6" ] [ text <| stageToString digimon.stage ]
            ]
        , requirementsView digimon.requirements
        , evolutionsView digimon.evolutions True
        ]
