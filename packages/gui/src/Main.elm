module Main exposing (main)

import Browser
import Browser.Navigation as Nav
import Digimon exposing (Digimon, digimonDecoder)
import Html exposing (..)
import Html.Attributes as Attr
import Html.Events as Events
import Json.Decode as Decode exposing (Value, decodeValue)
import Ports exposing (..)
import RemoteData exposing (RemoteData)
import Url


main : Program Value Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = UrlRequested
        , onUrlChange = UrlChanged
        }


type alias Model =
    { key : Nav.Key
    , url : Url.Url
    , digimon : RemoteData String Digimon
    , query : String
    }


modelInitialValue : Url.Url -> Nav.Key -> Model
modelInitialValue url key =
    { key = key
    , url = url
    , digimon = RemoteData.NotAsked
    , query = ""
    }


init : flags -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init _ url key =
    ( modelInitialValue url key, Cmd.none )


type Msg
    = GotEvolutionTree Value
    | GetEvolutionTree String
    | DigimonNotFound String
    | QueryChanged String
    | UrlRequested Browser.UrlRequest
    | UrlChanged Url.Url


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotEvolutionTree value ->
            case decodeValue digimonDecoder value of
                Ok digimon ->
                    ( { model | digimon = RemoteData.Success digimon, query = "" }, Cmd.none )

                Err e ->
                    ( { model | digimon = RemoteData.Failure (Decode.errorToString e) }, Cmd.none )

        GetEvolutionTree name ->
            ( { model | digimon = RemoteData.Loading }, getEvolutionTree name )

        DigimonNotFound name ->
            ( { model | digimon = RemoteData.Failure ("Digimon not found: " ++ name) }, Cmd.none )

        QueryChanged q ->
            ( { model | query = q }, Cmd.none )

        UrlRequested urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            ( { model | url = url }
            , Cmd.none
            )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch
        [ gotEvolutionTree GotEvolutionTree
        , digimonNotFound DigimonNotFound
        ]


view : Model -> Browser.Document Msg
view model =
    { title = "Digimon World: Next Order Evolution Chart"
    , body =
        [ div []
            [ h1 [] [ text "Digimon World: Next Order Evolution Chart" ]
            , a [ Attr.class "h6", Attr.href "https://gitlab.com/gumbyscout/next-order-evolutions/-/issues/new" ] [ text "Submit issues to our gitlab repo" ]
            , form [ Events.onSubmit <| GetEvolutionTree model.query ]
                [ label [ Attr.class "form-label", Attr.for "digimon-name" ] [ text "Digimon Name" ]
                , div [ Attr.class "input-group" ]
                    [ input
                        [ Attr.type_ "text"
                        , Attr.id "digimon-name"
                        , Attr.class "form-input"
                        , Events.onInput QueryChanged
                        ]
                        []
                    , button [ Attr.type_ "submit", Attr.class "btn input-group-btn" ] [ text "Search" ]
                    ]
                ]
            , case model.digimon of
                RemoteData.NotAsked ->
                    div [ Attr.class "empty" ]
                        [ div [ Attr.class "empty-icon" ]
                            [ i [ Attr.class "icon icon-message icon-4x" ] []
                            ]
                        , p [ Attr.class "empty-title h5" ] [ text "Search for a Digimon to begin" ]
                        ]

                RemoteData.Failure e ->
                    text ("An error occured " ++ e)

                RemoteData.Success digimon ->
                    Digimon.view digimon

                RemoteData.Loading ->
                    div [ Attr.class "loading loading-lg" ] []
            ]
        ]
    }
