import { Elm } from "./Main.elm";

import { getEvolutionTree } from "next-order-evolutions-source";

const capitalize = (s = "") => s.charAt(0).toUpperCase() + s.slice(1);

const app = Elm.Main.init({
  node: document.querySelector("main"),
});

app.ports.getEvolutionTree.subscribe(async (name) => {
  const cleanName = capitalize(name).trim();
  const tree = await getEvolutionTree(cleanName);
  if (!tree) {
    app.ports.digimonNotFound.send(cleanName);
    return;
  }
  app.ports.gotEvolutionTree.send(tree);
});
