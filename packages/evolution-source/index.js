const DataStore = require("nedb");
const parse = require("csv-parse/lib/sync");

const csvs = require("./csvs");
const { megas } = require("./supplementary");

const EMPTY_COLUMN = "-";

/**
 * Requirements for a specific evolution
 * @typedef {Object} EvolutionRequirements
 * @property {string} [hp]
 * @property {string} [mp]
 * @property {string} [strength]
 * @property {string} [stamina]
 * @property {string} [wisdom]
 * @property {string} [speed]
 * @property {string} [weight]
 * @property {string} [mistakes]
 * @property {string} [bond]
 * @property {string} [discipline]
 * @property {string} [wins]
 * @property {string} [keyDigimon]
 * @property {string} [keyPoints]
 */

/**
 * An evolution that a specfic digimon can evolve into.
 * @typedef {Object} Evolution
 * @property {string} name
 * @property {EvolutionRequirements} requirements
 */

/**
 * A digimon and the evolutions it can evolve into. The denormalized type for the db.
 * @typedef {Object} Digimon
 * @property {string} name
 * @property {string} stage - the current evolution stage
 * @property {Array<Evolution>} evolutions
 */

/**
 * A denormalized node in an evolution tree.
 * @typedef {Object} EvolutionNode
 * @property {string} name
 * @property {string} stage - the current evolution stage
 * @property {Array<EvolutionNode>} evolutions
 * @property {EvolutionRequirements} requirements
 */

/**
 * Reads a csv containing evolution information, and returns the array of digimon
 * @param {string} csv
 * @param {string} stage - the stage of this level of digimon
 * @returns {Array<Digimon>}
 */
const readCSV = (csv, stage) => {
  const rows = parse(csv, {
    columns: [
      "name",
      "evolution",
      "hp",
      "mp",
      "strength",
      "stamina",
      "wisdom",
      "speed",
      "weight",
      "mistakes",
      "bond",
      "discipline",
      "wins",
      "keyDigimon",
      "keyPoints",
    ],
  });

  // First row is column names
  rows.shift();

  // This loop is impure
  const digimonList = [];
  for (var i = 0; i < rows.length; i++) {
    const {
      name,
      evolution,
      hp,
      mp,
      strength,
      stamina,
      wisdom,
      speed,
      weight,
      mistakes,
      bond,
      discipline,
      wins,
      keyDigimon,
      keyPoints,
    } = rows[i];

    var digimon = { name, evolutions: [], stage };
    var digimonExists = false;
    // Empty names mean to add to previous digimon
    if (name === EMPTY_COLUMN) {
      var last_index = i - 1;
      while (
        !digimonList[last_index] ||
        digimonList[last_index].name === EMPTY_COLUMN
      ) {
        last_index--;
      }
      digimon = digimonList[last_index];
      digimonExists = true;
    }

    const requirements = {
      hp,
      mp,
      strength,
      stamina,
      wisdom,
      speed,
      weight,
      mistakes,
      bond,
      discipline,
      wins,
      keyDigimon,
      keyPoints,
    };

    // Clean up empty columns
    for (var prop in requirements) {
      const value = requirements[prop];
      if (value == EMPTY_COLUMN || value == "") {
        delete requirements[prop];
      }
    }

    // Add the evolution to the evolutions
    digimon.evolutions.push({
      name: evolution,
      requirements,
    });
    if (!digimonExists) digimonList.push(digimon);
  }

  return digimonList;
};

/**
 * Wraps a standard node callback in a promise
 * @param {Promise|object} obj - a normal object or promise
 * @param {string} property
 * @param {...any} args
 */
const promisfy = (obj, property, ...args) =>
  Promise.resolve(obj).then(
    (obj) =>
      new Promise((resolve, reject) => {
        obj[property](...args, (err, docs) => {
          if (err) {
            reject(err);
          }
          resolve(docs);
        });
      })
  );

/**
 * Get the whole evolution tree for a specific digimon
 * @param {string} name - the digimon's name, capitalized e.g. "Agumon"
 * @param {Nedb<Digimon>} [db] - the digimon db to query
 * @returns {Promise<EvolutionNode>}
 */
const getEvolutionTree = async (name, db = defaultDB) => {
  // There should only be one named digimon
  const digimon = await promisfy(db, "findOne", { name });
  if (!digimon) {
    return;
  }
  const tree = { name, evolutions: [], requirements: {}, stage: digimon.stage };

  // This means the digimon is a mega, and doesn't evolve.
  if (!Array.isArray(digimon.evolutions)) {
    return digimon;
  }

  // Step through and fetch associated evolutions.
  for (var evolution of digimon.evolutions) {
    const { name, requirements } = evolution;
    const subTree = await getEvolutionTree(name, db);
    tree.evolutions.push({
      name,
      stage: subTree && subTree.stage,
      requirements,
      evolutions: subTree && subTree.evolutions,
    });
  }

  return tree;
};

/**
 * Returns the next evolutions for a digimon
 * @param {string} name
 * @param {Nedb<Digimon>} db
 * @returns {Promise<Array<Evolution>>}
 */
const getEvolutions = async (name, db = defaultDB) => {
  const digimon = await promisfy(db, "findOne", { name });
  if (!digimon) {
    return [];
  }
  return digimon.evolutions;
};

/**
 * Seed the db from the csv source files.
 * @param {Array<string>} [sourceCSVS] - the csv files to load
 * @returns {Promise<Nedb<Digimon>>}
 */
const seedDB = async (sourceCSVS = csvs) => {
  const db = new DataStore();

  for (var stage in sourceCSVS) {
    const digimon = readCSV(sourceCSVS[stage], stage);
    await promisfy(db, "insert", digimon);
  }

  // Add megas too
  await promisfy(
    db,
    "insert",
    megas.map((mega) => ({
      name: mega,
      stage: "mega",
    }))
  );

  // TODO: Figure out why the `unique` constraint breaks
  await promisfy(db, "ensureIndex", { fieldName: "name" });

  return db;
};

const defaultDB = seedDB();

module.exports = {
  readCSV,
  getEvolutionTree,
  seedDB,
  promisfy,
  getEvolutions,
};
