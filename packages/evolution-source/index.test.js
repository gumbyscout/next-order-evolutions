const { readCSV, getEvolutionTree, seedDB } = require(".");

const csvs = require("./csvs");

const testCSV = `Name,Evolution,HP,MP,Strength,Stamina,Wisdom,Speed,Weight,Mistakes,Bond,Discipline,Wins,Key Digimon,Key Points
Foo,Bar,-,-,-,-,-,-,-,-,-,-,-,-,-
-,Bar2,-,-,-,-,-,-,-,-,-,-,-,-,-
-,Bar3,-,-,-,-,-,-,-,-,-,-,-,-,-
Bar,Baz,-,-,-,-,-,-,-,-,-,-,-,-,-
-,Baz2,-,-,-,-,-,-,-,-,-,-,-,-,-
Baz,Fiz,-,-,-,-,-,-,-,-,-,-,-,-,-`;

describe("readCSV", () => {
  test("returns a list of digimon", () => {
    const res = readCSV(csvs.rookie);
    const expectedFirstDigimon = {
      name: "Agumon",
      evolutions: [
        // Agumon,Greymon,14400,-,1500,1500,-,-,20,≤3,50,-,15,Agumon,5
        {
          name: "Greymon",
          requirements: {
            hp: "14400",
            strength: "1500",
            stamina: "1500",
            weight: "20",
            mistakes: "≤3",
            bond: "50",
            wins: "15",
            keyDigimon: "Agumon",
            keyPoints: "5",
          },
        },
        // -,Tyrannomon,1200,-,150,150,-,-,20,-,-,,15,-,
        {
          name: "Tyrannomon",
          requirements: {
            hp: "1200",
            strength: "150",
            stamina: "150",
            weight: "20",
            wins: "15",
          },
        },
        // -,Meramon,-,2400,350,-,-,250,≤19,≤5,-,-,15,-,4
        {
          name: "Meramon",
          requirements: {
            mp: "2400",
            strength: "350",
            speed: "250",
            weight: "≤19",
            mistakes: "≤5",
            wins: "15",
            keyPoints: "4",
          },
        },
        // -,Greymon (Blue),3400,-,500,-,-,-,20,1,50,≤49,15,-,4
        {
          name: "Greymon (Blue)",
          requirements: {
            hp: "3400",
            strength: "500",
            weight: "20",
            mistakes: "1",
            bond: "50",
            discipline: "≤49",
            wins: "15",
            keyPoints: "4",
          },
        },
      ],
    };
    expect(res[0]).toEqual(expectedFirstDigimon);

    const res2 = readCSV(testCSV);

    const expected2 = [
      // Foo,Bar,-,-,-,-,-,-,-,-,-,-,-,-,-
      // -,Bar2,-,-,-,-,-,-,-,-,-,-,-,-,-
      // -,Bar3,-,-,-,-,-,-,-,-,-,-,-,-,-
      {
        name: "Foo",
        evolutions: [
          {
            name: "Bar",
            requirements: {},
          },
          {
            name: "Bar2",
            requirements: {},
          },
          {
            name: "Bar3",
            requirements: {},
          },
        ],
      },
      // Bar,Baz,-,-,-,-,-,-,-,-,-,-,-,-,-
      // -,Baz2,-,-,-,-,-,-,-,-,-,-,-,-,-
      {
        name: "Bar",
        evolutions: [
          {
            name: "Baz",
            requirements: {},
          },
          {
            name: "Baz2",
            requirements: {},
          },
        ],
      },
      // Baz,Fiz,-,-,-,-,-,-,-,-,-,-,-,-,-
      {
        name: "Baz",
        evolutions: [
          {
            name: "Fiz",
            requirements: {},
          },
        ],
      },
    ];
    expect(res2).toEqual(expected2);
  });
});

describe("getEvolutionTree", () => {
  test("returns a tree of evolutions", async () => {
    const db = await seedDB({ testCSV });
    const expected = {
      name: "Foo",
      stage: "testCSV",
      evolutions: [
        {
          name: "Bar",
          stage: "testCSV",
          evolutions: [
            {
              name: "Baz",
              stage: "testCSV",
              evolutions: [
                {
                  name: "Fiz",
                  requirements: {},
                },
              ],
              requirements: {},
            },
            {
              name: "Baz2",
              requirements: {},
            },
          ],
          requirements: {},
        },
        {
          name: "Bar2",
          requirements: {},
        },
        {
          name: "Bar3",
          requirements: {},
        },
      ],
      requirements: {},
    };
    const tree = await getEvolutionTree("Foo", db);
    expect(tree).toEqual(expected);
  });

  test("doesn't return bunk data on digimon not found", () => {
    expect(getEvolutionTree("Foo")).resolves.toBeFalsy();
  });
});
