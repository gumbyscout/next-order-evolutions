// Copied CSV files into js to avoid module resolution failure.
const baby = `Name,Evolution,HP,MP,Strength,Stamina,Wisdom,Speed,Weight,Mistakes,Bond,Discipline,Wins,Key Digimon,Key Points
Botamon,Koromon,,,,,,,,,,,,,
Punimon,Tsunomon,,,,,,,,,,,,,
Poyomon,Tokomon,,,,,,,,,,,,,
YukimiBotamon,Nyaromon,,,,,,,,,,,,,
Pabumon,Motimon,,,,,,,,,,,,,
Jyarimon,Gigimon,,,,,,,,,,,,,
Zerimon,Gummymon,,,,,,,,,,,,,
Conomon,Kokomon,,,,,,,,,,,,,
Pichimon,Bukamon,,,,,,,,,,,,,
Yuramon,Tanemon,,,,,,,,,,,,,
Kuramon,Tsumemon,,,,,,,,,,,,,`;

const inTraining = `Name,Evolution,HP,MP,Strength,Stamina,Wisdom,Speed,Weight,Mistakes,Bond,Discipline,Wins,Key Digimon,Key Points
Koromon,Agumon,1900,-,890,-,-,-,-,-,-,-,-,-,2
-,Agumon (Black),150,-,10,40,-,-,-,-,-,-,-,-,3
-,ToyAgumon,-,1000,-,80,-,-,-,-,-,-,-,-,2
-,Veemon,-,-,210,-,-,150,-,-,-,-,-,-,2
-,Shoutmon,2000,1000,480,100,100,100,-,-,-,-,-,-,6
Tsunomon,Gabumon,-,-,-,530,550,-,-,-,-,-,-,-,2
-,Gaomon,-,-,180,180,-,-,-,-,-,-,-,-,2
-,Psychemon,-,300,50,.,50,50,-,-,-,-,-,-,3
-,Gabumon (Black),150,-,-,25,25,-,-,-,-,-,-,-,3
-,Gumdramon,1000,2000,100,100,100,480,-,-,-,-,-,-,6
Tokomon,Patamon,600,-,-,-,300,-,-,-,-,-,-,-,2
-,Biyomon,-,600,-,-,-,300,-,-,-,-,-,-,2
-,Tsukaimon,150,100,-,-,40,,-,-,-,-,-,-,3
-,Hackmon,1500,3000,450,750,750,450,-,-,-,-,-,-,6
-,Lucemon,5000,5000,2000,2000,2000,2000,-,-,-,-,-,-,6
Nyaromon,Salamon,-,-,-,-,150,210,-,-,-,-,-,-,2
-,SnowAgumon,-,300,-,50,50,50,-,-,-,-,-,-,3
-,Renamon,-,1000,-,-,80,-,-,-,-,-,-,-,2
-,Biyomon,-,600,-,-,-,300,-,-,-,-,-,-,2
-,Lucemon,5000,5000,2000,2000,2000,2000,-,-,-,-,-,-,6
Motimon,Tentomon,1500,-,-,210,-,-,-,-,-,-,-,-,2
-,Wormmon,1000,-,-,-,80,-,-,-,-,-,-,-,2
-,Gotsumon,500,-,-,130,-,-,-,-,-,-,-,-,2
-,Hackmon,1500,3000,450,750,750,450,-,-,-,-,-,-,6
-,Hagurumon,-,-,-,80,100,-,-,-,-,-,-,-,2
Gigimon,Guilmon,1900,-,590,300,-,-,-,-,-,-,-,-,2
-,Goblimon,-,-,80,-,-,100,-,-,-,-,-,-,2
-,Solarmon,150,150,35,-,-,-,-,-,-,-,-,-,3
-,Gotsumon,500,-,-,130,-,-,-,-,-,-,-,-,2
-,Shoutmon,2000,1000,480,100,100,100,-,-,-,-,-,-,6
Gummymon,Terriermon,-,-,-,150,-,210,-,-,-,-,-,-,2
-,ToyAgumon,-,1000,-,80,-,-,-,-,-,-,-,-,2
-,ClearAgumon,300,-,-,50,50,50,-,-,-,-,-,-,3
-,Hagurumon,-,-,-,80,100,-,-,-,-,-,-,-,2
-,Gaomon,-,-,180,180,-,-,-,-,-,-,-,-,2
Kokomon,Lopmon,-,-,50,-,130,-,-,-,-,-,-,-,2
-,Demidevimon,-,500,-,-,130,-,-,-,-,-,-,-,2
-,ToyAgumon (Black),-,150,40,10,-,-,-,-,-,-,-,-,3
-,Shamanmon,-,-,15,-,50,-,-,-,-,-,-,-,2
-,Wormmon,1000,-,-,-,80,-,-,-,-,-,-,-,2
Bukamon,Gomamon,-,-,50,130,-,-,-,-,-,-,-,-,2
-,Veemon,-,-,210,-,-,150,-,-,-,-,-,-,2
-,SnowGoburimon,500,500,-,30,-,50,-,-,-,-,-,-,2
-,ClearAgumon,300,-,-,50,50,50,-,-,-,-,-,-,3
-,Gumdramon,1000,2000,100,100,100,480,-,-,-,-,-,-,6
Tanemon,Palmon,800,1000,-,-,-,-,-,-,-,-,-,-,2
-,Renamon,-,1000,-,-,80,-,-,-,-,-,-,-,2
-,Aruraumon,-,150,20,-,30,-,-,-,-,-,-,-,2
-,Goblimon,-,-,80,-,-,100,-,-,-,-,-,-,2
-,Demidevimon,-,500,-,-,130,-,-,-,-,-,-,-,2
Tsumemon,Keramon,-,-,600,600,750,900,-,-,-,-,-,-,3
-,ToyAgumon (Black),-,150,40,10,-,-,-,-,-,-,-,-,3
-,Agumon (Black),150,-,10,40,-,-,-,-,-,-,-,-,3
-,Gabumon (Black),150,-,-,25,25,-,-,-,-,-,-,-,3
-,Tsukaimon,150,100,-,-,40,-,-,-,-,-,-,-,3`;

const rookie = `Name,Evolution,HP,MP,Strength,Stamina,Wisdom,Speed,Weight,Mistakes,Bond,Discipline,Wins,Key Digimon,Key Points
Agumon,Greymon,14400,-,1500,1500,-,-,20,≤3,50,-,15,Agumon,5
-,Tyrannomon,1200,-,150,150,-,-,20,-,-,,15,-,
-,Meramon,-,2400,350,-,-,250,≤19,≤5,-,-,15,-,4
-,Greymon (Blue),3400,-,500,-,-,-,20,1,50,≤49,15,-,4
Gabumon,Garurumon,14400,-,-,1500,-,1500,≤19,≤3,50,,15,Gabumon,5
-,Gaogamon,4800,-,400,-,-,500,≤24,≤3,50,-,20,Gaomon,5
-,Garurumon (Black),2400,1000,,-,-,500,≤19,1,-,≤49,15,,4
-,Seadramon,1000,700,,-,-,250,20,≤5,-,,15,-,4
Biyomon,Birdramon,-,4800,400,-,-,500,≤14,≤5,50,50,-,Biyomon,5
-,Piddomon,-,2400,-,,300,300,≤19,≤3,-,50,-,-,4
-,RedVeedramon,4800,-,550,350,-,-,20,≤1,50,-,20,-,5
-,Youkomon,-,2400,-,-,400,200,≤19,≤5,-,50,-,-,4
Patamon,Angemon,-,4800,-,-,500,400,≤14,≤3,50,50,-,Patamon,5
-,Piddomon,-,2400,-,,300,300,≤19,≤3,-,50,-,-,4
-,Veedramon,4800,-,500,400,-,-,20,≤5,50,-,15,-,5
-,Growlmon (Orange),2400,-,-,200,200,200,20,-,-,-,15,-,4
Tentomon,Kabuterimon,-,-,500,500,-,380,20,≤5,-,50,20,Tentomon,5
-,RedVegiemon,1700,1700,500,-,-,-,≤19,1,-,≤49,-,-,
-,Woodmon,1400,1000,300,300,-,-,≤19,≤5,-,-,-,-,4
-,BomberNanimon,4800,-,-,250,250,400,-,1,-,≤29,-,-,
Gomamon,Ikkakumon,1700,1700,-,500,-,,25,,-,≤49,15,Gomamon,5
-,Seadramon,1000,700,,-,-,250,20,≤5,-,,15,-,4
-,Icemon,-,2400,250,350,-,-,20,-,-,≤49,15,-,4
-,Sorcermon,-,2400,-,-,350,250,≤19,≤3,-.,50,-,-,4
Palmon,Togemon,2400,-,300,-,300,-,20,≤5,50,-,-,Palmon,5
-,Woodmon,1400,1000,300,300,-,-,≤19,≤5,-,-,-,-,4
-,Vegiemon,600,600,-,-,300,,≤19,3,-,≤29,,-,4
-,RedVegiemon,1700,1700,500,-,-,-,≤19,1,-,≤49,-,-,
Salamon,Gatomon,-,7800,-,-,600,-,≤14,≤3,50,50,15,Salamon,5
-,BlackGatomon,-,4800,-,-,450,450,≤14,1,-,≤49,-,-,4
-,Gargomon,-,4800,-,500,-,400,25,≤5,-,≤49,15,Terriermon,5
-,Meicoomon,-,-,1320,-,1320,1800,≤14,-,50,≤29,-,-,5
Veemon,ExVeemon,14400,-,1500,-,-,1500,≤20,≤3,50,-,15,Veemon,5
-,Veedramon,4800,-,500,400,-,-,20,≤5,50,-,15,-,5
-,Meramon,-,2400,350,-,-,250,≤19,≤5,-,-,15,-,4
-,GoldVeedramon,-,27000,2400,1800,1800,-,-,-,75,50,20,-,5
Wormmon,Stingmon,14400,-,-,1500,1500,-,-,≤5,50,50,20,Wormmon,5
-,Kuwagamon,2400,-,450,150,-,-,20,-,-,≤49,20,-,4
-,Kabuterimon,-,-,500,500,-,380,20,≤5,-,50,20,Tentomon,5
-,Devimon,-,2400,400,-,200,-,≤19,1,-,≤49,15,-,5
Guilmon,Growlmon,-,-,1640,1600,-,1200,20,-,50,≤49,20,Guilmon,5
-,Growlmon (Orange),2400,-,,200,200,200,20,-,-,-,15,-,4
-,Tyrannomon,1200,-,150,150,-,-,20,-,-,,15,-,4
-,RedVeedramon,4800,-,550,350,-,-,20,≤1,50,-,20,-,5
Terriermon,Gargomon,-,4800,-,500,-,400,25,≤5,-,≤49,15,Terriermon,5
-,Angemon,-,4800,-,-,500,400,≤14,≤3,50,50,-,Patamon,5
-,Ikkakumon,1700,1700,-,500,-,,25,,-,≤49,15,Gomamon,5
-,Ogremon,1200,,200,100,-,-,20,1,-,≤29,20,-,5
Lopmon,Turuiemon,-,-,300,-,300,240,≤19,1,-,≤49,15,Lopmon,5
-,Kyuubimon (Silver),-,2400,-,-,350,250,≤19,≤5,-,50,-,-,4
-,Wizardmon,-,2400,200,-,400,-,≤19,≤5,-,-,15,-,5
-,BlackGatomon,-,4800,-,-,450,450,≤14,1,-,≤49,-,-,4
Renamon,Kyuubimon,,2400,,-,300,300,-,≤3,50,,-,Renamon,
-,Kyuubimon (Silver),-,2400,-,-,350,250,≤19,≤5,-,50,-,-,4
-,Turuiemon,-,-,300,-,300,240,≤19,1,-,≤49,15,Lopmon,5
-,Leomon,4800,-,250,250,-,400,≤24,≤3,-,-,15,,5
Gaomon,Gaogamon,4800,-,400,-,-,500,≤24,≤3,50,-,20,Gaomon,5
-,Garurumon,14400,-,-,1500,-,1500,≤19,≤3,50,,15,Gabumon,5
-,Leomon,4800,-,250,250,-,400,≤24,≤3,-,-,15,,5
-,Togemon,2400,-,300,-,300,-,20,≤5,50,-,-,Palmon,5
Hagurumon,Guardromon,,200,100,100,,-,-,-,50,50,-,,4
-,GoldVeedramon,-,27000,2400,1800,1800,-,-,-,75,50,20,-,5
-,Kuwagamon,2400,-,450,150,-,-,20,-,-,≤49,20,-,4
-,BomberNanimon,4800,-,-,250,250,400,-,1,-,≤29,-,-,
Gotsumon,Icemon,-,2400,250,350,-,-,20,-,-,≤49,15,-,4
-,Ogremon,1200,,200,100,-,-,20,1,-,≤29,20,-,5
-,Tyrannomon,1200,-,150,150,-,-,20,-,-,,15,-,4
,Nanimon,-,-,,150,-,120,-,3,-,≤29,-,-,4
Goblimon,Ogremon,1200,,200,100,-,-,20,1,-,≤29,20,-,5
-,Nanimon,-,-,,150,-,120,-,3,-,≤29,-,-,4
-,Woodmon,1400,1000,300,300,-,-,≤19,≤5,-,-,-,-,4
-,Gaogamon,4800,-,400,-,-,500,≤24,≤3,50,-,20,Gaomon,5
ToyAgumon,Wizardmon,-,2400,200,-,400,-,≤19,≤5,-,-,15,-,5
-,Greymon,14400,-,1500,1500,-,-,20,≤3,50,-,15,Agumon,5
-,Guardromon (Gold),-,-,1240,1600,1600,-,20,≤3,50,-,-,-,5
,Growlmon (Orange),2400,-,-,200,200,200,20,-,-,-,15,-,4
Demidevimon,Devimon,-,2400,400,-,200,-,≤19,1,-,≤49,15,-,5
-,BlackGatomon,-,4800,-,-,450,450,≤14,1,-,≤49,-,-,4
-,IceDevimon,-,2400,-,-,300,300,-,1,-,≤49,15,-,4
-,Chrysalimon,-,27000,1800,-,2400,1800,≤14,3,-,-,20,-,6
Lucemon,IceDevimon,-,2400,-,-,300,300,-,1,-,≤49,15,-,4
-,Piddomon,-,2400,-,,300,300,≤19,≤3,-,50,-,-,4
-,Wizardmon,-,2400,200,-,400,-,≤19,≤5,-,-,15,-,5
Hackmon,Leomon,4800,-,250,250,-,400,≤24,≤3,-,-,15,,5
-,Gururumon,2400,,250,-,-,350,≤19,-,-,≤49,15,,4
-.,ExVeemon,14400,-,1500,-,-,1500,≤20,≤3,50,-,15,Veemon,5
-,GoldVeedramon,-,27000,2400,1800,1800,-,-,-,75,50,20,-,5
Agumon (Black),Greymon (Blue),3400,-,500,-,-,-,20,1,50,≤49,15,-,4
-,Growlmon,-,-,1640,1600,-,1200,20,-,50,≤49,20,Guilmon,5
-,Saberdramon,-,1200,75,-,75,150,≤14,-,-,-,15,-,4
,Fugamon,2400,-,-,-,-,600,20,1,-,≤29,15,-,4
SnowAgumon,Sorcermon,-,2400,-,-,350,250,≤19,≤3,-.,50,-,-,4
-,Gatomon,-,7800,-,-,600,-,≤14,≤3,50,50,15,Salamon,5
-,Hyogamon,1400,1000,600,-,-,-,20,1,-,-,15,-,4
-,Icemon,-,2400,250,350,-,-,20,-,-,≤49,15,-,4
Gabumon (Black),Garurumon (Black),2400,1000,,-,-,500,≤19,1,-,≤49,15,,4
-,Stingmon,14400,-,-,1500,1500,-,-,≤5,50,50,20,Wormmon,5
-,Gururumon,2400,,250,-,-,350,≤19,-,-,≤49,15,,4
,Saberdramon,-,1200,75,-,75,150,≤14,-,-,-,15,-,4
Psychemon,Gururumon,2400,,250,-,-,350,≤19,-,-,≤49,15,,4
-,Kyuubimon,,2400,,-,300,300,-,≤3,50,,-,Renamon,
-,BomberNanimon,4800,-,-,250,250,400,-,1,-,≤29,-,-,
,Gargomon,-,4800,-,500,-,400,25,≤5,-,≤49,15,Terriermon,5
Tsukaimon,Saberdramon,-,1200,75,-,75,150,≤14,-,-,-,15,-,4
-,Devimon,-,2400,400,-,200,-,≤19,1,-,≤49,15,-,5
-,Youkomon,-,2400,-,-,400,200,≤19,≤5,-,50,-,-,4
,Garurumon (Black),2400,1000,,-,-,500,≤19,1,-,≤49,15,,4
Aruraumon,Vegiemon,600,600,-,-,300,,≤19,3,-,≤29,,-,4
-,Youkomon,-,2400,-,-,400,200,≤19,≤5,-,50,-,-,4
-,Togemon,2400,-,300,-,300,-,20,≤5,50,-,-,Palmon,5
-,Stingmon,14400,-,-,1500,1500,-,-,≤5,50,50,-,Wormmon,5
ToyAgumon (Black),Nanimon,-,-,,150,-,120,-,3,-,≤29,-,-,4
-,Guardromon,,200,100,100,,-,-,-,50,50,-,,4
-,Fugamon,2400,-,-,-,-,600,20,1,-,≤29,15,-,4
-,Greymon (Blue),3400,-,500,-,-,-,20,1,50,≤49,15,-,4
ClearAgumon,Guardromon (Gold),-,-,1240,1600,1600,-,20,≤3,50,,-,-,5
-,IceDevimon,-,2400,-,-,300,300,-,1,-,≤49,15,-,4
-,ExVeemon,14400,-,1500,-,-,1500,≤20,≤3,50,-,15,Veemon,5
-,Hyogamon,1400,1000,600,-,-,-,20,1,-,-,15,-,4
Solarmon,Meramon,-,2400,350,-,-,250,≤19,≤5,-,-,15,-,4
-,RedVegiemon,1700,1700,500,-,-,-,≤19,1,-,≤49,-,-,
-,Birdramon,-,4800,400,-,-,500,≤14,≤5,50,50,-,Biyomon,5
-,Guardromon (Gold),-,-,1240,1600,1600,-,20,≤3,50,,-,-,5
SnowGoburimon,Hyogamon,1400,1000,600,-,-,-,20,1,-,-,15,-,4
-,Kyuubimon (Silver),-,2400,-,-,350,250,≤19,≤5,-,50,-,-,4
-,Ikkakumon,1700,1700,-,500,-,,25,,-,≤49,15,Gomamon,5
-,Seadramon,1000,700,,-,-,250,20,≤5,-,,15,-,4
Shamanmon,Fugamon,2400,-,-,-,-,600,20,1,-,≤29,15,-,4
-,Vegiemon,600,600,-,-,300,,≤19,3,-,≤29,,-,4
-,Kuwagamon,2400,-,450,150,-,-,20,-,-,≤49,20,-,4
-,Sorcermon,-,2400,-,-,350,250,≤19,≤3,-.,50,-,-,4
Keramon,Chrysalimon,-,27000,1800,-,2400,1800,≤14,3,-,-,20,-,6
-,Guardromon,,200,100,100,,-,-,-,50,50,-,,4
-,Meicoomon,-,-,1320,-,1320,1800,≤14,-,50,≤29,-,-,5
-,Kabuterimon,-,-,500,500,-,380,20,≤5,-,50,20,Tentomon,5
Gumdramon,Arresterdramon,11400,3000,1000,600,,800,-,,50,-,-,,6
-,Veedramon,4800,-,500,400,-,-,20,≤5,50,-,15,-,5
,Turuiemon,-,-,300,-,300,240,≤19,1,-,≤49,15,Lopmon,5
,Gatomon,-,7800,-,-,600,-,≤14,≤3,50,50,15,Salamon,5
Shoutmon,OmniShoutmon,3000,11400,400,600,1000,1000,-,-,50,-,-,-,6
-,Growlmon,-,-,1640,1600,-,1200,20,-,50,≤49,20,Guilmon,5
-,Greymon,14400,-,1500,1500,-,-,20,≤3,50,-,15,Agumon,5
-,RedVeedramon,4800,-,550,350,-,-,20,≤1,50,-,20,-,5`;

const champion = `Name,Evolution,HP,MP,Strength,Stamina,Wisdom,Speed,Weight,Mistakes,Bond,Discipline,Wins,Key Digimon,Key Points
Greymon,MetalGreymon,28000,-,3300,2100,-,2100,40,≤1,70,-,30,Greymon,7
-,MetalGreymon (Blue),6000,-,700,500,-,600,40,-,-,≤49,30,Greymon (Blue),6
-,WarGrowlmon (Orange),1000,2500,600,700,-,-,40,≤2,-,40,30,Growlmon (Orange),6
Garurumon,WereGarurumon,19000,19000,-,-,3000,3500,≤39,≤1,70,40,-,Garurumon,7
-,GrapLeomon,4000,2000,900,-,-,900,35,≤2,70,-,30,Leomon,6
-,MachGaogamon,-,9500,900,-,500,900,≤39,≤1,70,-,30,Gaogamon,6
Birdramon,Garudamon,-,9500,-,500,800,1000,≤39,≤2,70,40,-,Birdramon,6
-,AeroVeedramon,9500,-,800,700,-,800,35,≤2,70,-,30,Veedramon,6
-,Agunimon,54000,-,4800,3900,-,3900,≤39,≤1,70,-,30,-,7
Angemon,MagnaAngemon,9500,-,650,650,1000,0,≤39,≤1,70,70,0,Angemon,7
-,BlueMeramon,3000,2000,400,-,350,400,≤35,-,-,-,30,Meramon,6
-,Taomon,-,6000,600,-,700,500,≤39,,,,-,Kyuubimon,
Kabuterimon,MegaKabuterimon,9500,-,800,750,-,750,40,≤2,70,-,30,Kabuterimon,6
-,MegaKabuterimon (Blue),2000,1500,300,300,400,300,40,-,-,≤49,30,Kabuterimon,6
-,SkullGreymon,3000,3000,900,900,-,-,≤44,5,-,≤39,30,-,6
Ikkakumon,Zudomon,6000,-,800,500,500,,,≤2,70,-,30,,6
-,IceLeomon,28000,-,2600,2600,-,2300,-,≤2,70,60,30,-,6
-,Rapidmon,6000,-,600,-,600,600,≤39,≤1,40,,-,Gargomon,6
Togemon,Lillymon,-,6000,400,-,700,700,≤29,≤2,70,,-,Togemon,6
-,Mamemon,3500,-,500,350,-,450,≤20,-,-,≤59,30,-,6
-,LadyDevimon,-,6000,600,-,600,600,≤39,≤1,-,≤39,-,-,6
Gatomon,Angewomon,-,6000,600,-,700,500,≤34,≤1,70,70,-,Gatomon,7
-,Taomon (Silver),-,6000,450,300,600,450,≤34,≤2,,-,-,Kyuubimon (Silver),6
-,Lillymon,-,6000,400,-,700,700,≤29,≤2,70,,-,Togemon,6
ExVeemon,AeroVeedramon,9500,-,800,700,-,800,35,≤2,70,-,30,Veedramon,6
-,MetalTyrannomon,28000,-,3000,2600,-,1900,45,-,-,≤49,30,Tyrannomon,6
-,Garudamon,-,9500,-,500,800,1000,≤39,≤2,70,40,-,Birdramon,6
Stingmon,MegaKabuterimon (Blue),2000,1500,300,300,400,300,40,-,-,≤49,30,Kabuterimon,6
-,Okuwamon,6000,-,500,400,400,500,-,-,-,≤39,30,Kuwagamon,6
-,MegaKabuterimon,9500,-,800,750,-,750,40,≤2,70,-,30,Kabuterimon,6
Growlmon,WarGrowlmon,9500,-,800,800,-,700,40,-,70,≤49,30,Growlmon,7
-,SkullGreymon,3000,3000,900,900,-,-,≤44,5,-,≤39,30,-,6
-,BlackWarGrowlmon,6000,-,900,900,-,-,40,-,70,≤39,30,,6
Gargomon,Rapidmon,6000,-,600,-,600,600,≤39,≤1,40,,-,Gargomon,6
-,MetalMamemon,9500,-,700,900,700,-,≤25,-,-,≤59,30,-,6
-,Rapidmon (Gold),54000,,3600,2700,,3600,≤39,≤1,90,-,,,6
Turuiemon,Antylamon,-,-,-,-,750,900,≥35,≤1,70,70,-,Turuiemon,6
-,Megadramon,3500,3500,350,-,300,300,40,-,-,-,30,-,6
-,Monzaemon,6000,-,600,700,500,-,40,≤1,70,-,-,-,6
Kyuubimon,Taomon,-,6000,600,-,700,500,≤39,,,,-,Kyuubimon,
-,Monzaemon,6000,-,600,700,500,-,40,≤1,70,-,-,-,6
-,WereGarurumon,19000,19000,-,-,3000,3500,≤39,≤1,70,40,-,Garurumon,7
Gaogamon,MachGaogamon,-,9500,900,-,500,900,≤39,≤1,70,-,30,Gaogamon,6
-,WereGarurumon,19000,19000,-,-,3000,3500,≤39,≤1,70,40,-,Garurumon,7
-,GrapLeomon,4000,2000,900,-,-,900,35,≤2,70,-,30,Leomon,6
Wizardmon,Lucemon FM,40000,40000,5600,5600,5600,5600,-,0,-,≤29,40,-,7
-,Myotismon,28000,-,2300,-,2800,2400,-,≤1,-,≤39,30,-,6
-,MagnaAngemon,9500,-,650,650,1000,0,≤39,≤1,70,70,0,Angemon,7
Devimon,Myotismon,28000,-,2300,-,2800,2400,-,≤1,-,≤39,30,-,6
-,Doumon,3500,-,400,-,500,400,≤34,-,-,≤29,30,Youkomon,6
-,Antylamon,-,-,-,-,750,900,35,≤1,70,70,-,Turuiemon,6
Veedramon,AeroVeedramon,9500,-,800,700,-,800,35,≤2,70,-,30,Veedramon,6
-,MachGaogamon,-,9500,900,-,500,900,≤39,≤1,70,-,30,Gaogamon,6
-,MegaSeadramon,6000,-,450,450,550,350,40,-,-,-,30,Seadramon,6
Tyrannomon,MetalTyrannomon,28000,-,3000,2600,-,1900,45,-,-,≤49,30,Tyrannomon,6
-,MetalGreymon (Blue),6000,-,700,500,-,600,40,-,-,≤49,30,Greymon (Blue),6
-,WarGrowlmon,9500,-,800,800,-,700,40,-,70,≤49,30,Growlmon,7
Ogremon,SkullGreymon,3000,3000,900,900,-,-,≤44,5,-,≤39,30,-,6
-,WaruSeadramon,6000,-,400,400,500,500,40,-,-,≤29,30,-,6
-,MetalTyrannomon,28000,-,3000,2600,-,1900,45,-,-,≤49,30,Tyrannomon,6
Leomon,GrapLeomon,4000,2000,900,-,-,900,35,≤2,70,-,30,Leomon,6
-,Rapidmon,6000,-,600,-,600,600,≤39,≤1,40,,-,Gargomon,6
-,IceLeomon,28000,-,2600,2600,-,2300,-,≤2,70,60,30,-,6
Meramon,BlueMeramon,3000,2000,400,-,350,400,≤35,-,-,-,30,Meramon,6
-,Garudamon,-,9500,-,500,800,1000,≤39,≤2,70,40,-,Birdramon,6
-,Myotismon,28000,-,2300,-,2800,2400,-,≤1,-,≤39,30,-,6
Vegiemon,Mamemon,3500,-,500,350,-,450,≤20,-,-,≤59,30,-,6
-,Etemon,3500,-,400,400,-,500,≤39,-,-,≤29,30,-,6
-,Okuwamon,6000,-,500,400,400,500,-,-,-,≤39,30,Kuwagamon,6
Nanimon,WaruSeadramon,6000,-,400,400,500,500,40,-,-,≤29,30,-,6
-,Doumon,3500,-,400,,500,400,≤34,,,≤29,30,Youkomon,6
-,Etemon,3500,-,400,400,-,500,≤39,-,-,≤29,30,-,6
Kuwagamon,Okuwamon,6000,-,500,400,400,500,-,-,-,≤39,30,Kuwagamon,6
-,WarGrowlmon (Orange),1000,2500,600,700,-,-,40,≤2,-,40,30,Growlmon (Orange),6
-,Mamemon,3500,-,500,350,-,450,≤20,-,-,≤59,30,-,6
Seadramon,Gigadramon,3000,3000,600,600,-,600,40,-,-,-,30,-,6
-,MegaSeadramon,6000,-,450,450,550,350,40,-,-,-,30,Seadramon,6
-,WaruSeadramon,6000,-,400,400,500,500,40,-,-,≤29,30,-,6
Guardromon,Datamon,2000,4000,-,600,700,500,≤24,≤1,-,65,-,-,6
-,MetalMamemon,9500,-,700,900,700,-,≤25,-,-,≤59,30,-,6
-,MetalTyrannomon,28000,-,3000,2600,-,1900,45,-,-,≤49,30,Tyrannomon,6
Woodmon,Okuwamon,6000,-,500,400,400,500,-,-,-,≤39,30,Kuwagamon,6
-,Taomon,-,6000,600,-,700,500,≤39,,,,-,Kyuubimon,
-,Mamemon,3500,-,500,350,-,450,≤20,-,-,≤59,30,-,6
BomberNanimon,Megadramon,3500,3500,350,-,300,300,40,-,-,-,30,-,6
-,Meteormon,3500,-,300,400,300,300,45,≤2,-,-,25,,6
-,MetalMamemon,9500,-,700,900,700,-,≤25,-,-,≤59,30,-,6
Icemon,Mamemon,3500,-,500,350,-,450,≤20,-,-,≤59,30,-,6
-,Meteormon,3500,-,300,400,300,300,45,≤2,-,-,25,,6
-,Zudomon,6000,-,800,500,500,,,≤2,70,-,30,,6
Hyogamon,Zudomon,6000,-,800,500,500,,,≤2,70,-,30,,6
-,BlueMeramon,3000,2000,400,-,350,400,≤35,-,-,-,30,Meramon,6
-,IceLeomon,28000,-,2600,2600,-,2300,-,≤2,70,60,30,-,6
Piddomon,Meteormon,3500,-,300,400,300,300,45,≤2,-,-,25,,6
-,Angewomon,-,6000,600,-,700,500,≤34,≤1,70,70,-,Gatomon,7
-,Monzaemon,6000,-,600,700,500,-,40,≤1,70,-,-,-,6
Kyuubimon (Silver),Taomon (Silver),-,6000,450,300,600,450,≤34,≤2,,-,-,Kyuubimon (Silver),6
-,Gigadramon,3000,3000,600,600,-,600,40,-,-,-,30,-,6
-,Antylamon,-,-,-,-,750,900,35,≤1,70,70,-,Turuiemon,6
Gururumon,BlackWarGrowlmon,6000,-,900,900,-,-,40,-,70,≤39,30,,6
-,WereGarurumon (Black),6000,-,-,550,550,700,≤39,-,-,≤49,30,Garurumon(Black),6
-.,Taomon (Silver),-,6000,450,300,600,450,≤34,≤2,,-,-,Kyuubimon (Silver),6
Saberdramon,Megadramon,3500,3500,350,-,300,300,40,-,-,-,30,-,6
-,WaruSeadramon,6000,-,400,400,500,500,40,-,-,≤29,30,-,6
-,LadyDevimon,-,6000,600,-,600,600,≤39,≤1,-,≤39,-,-,6
BlackGatomon,LadyDevimon,-,6000,600,-,600,600,≤39,≤1,-,≤39,-,-,6
-,Antylamon,-,-,-,-,750,900,35,≤1,70,70,-,Turuiemon,6
-,Meicrackmon VM,3500,6000,500,500,600,700,≤29,-,70,-,30,-,7
Fugamon,BlackWarGrowlmon,6000,-,900,900,-,-,40,-,70,≤39,30,,6
-,WereGarurumon (Black),6000,-,-,550,550,700,≤39,-,-,≤49,30,Garurumon(Black),6
-,MachGaogamon,-,9500,900,-,500,900,≤39,≤1,70,,30,Gaogamon,6
Guardromon (Gold),MetalMamemon,9500,-,700,900,700,-,≤25,-,-,≤59,30,-,6
-,Datamon,2000,4000,-,600,700,500,≤24,≤1,-,65,-,-,6
-,Rapidmon (Gold),54000,,3600,2700,,3600,≤39,≤1,90,-,,,6
Youkomon,Doumon,3500,-,400,,500,400,≤34,,,≤29,30,Youkomon,6
-,MegaKabuterimon (Blue),2000,1500,300,300,400,300,40,-,-,≤49,30,Kabuterimon,6
-,WereGarurumon (Black),6000,-,-,550,550,700,≤39,-,-,≤49,30,Garurumon(Black),6
RedVeedramon,WarGrowlmon,9500,-,800,800,-,700,40,-,70,≤49,30,Growlmon,7
-,MegaKabuterimon,9500,-,800,750,-,750,40,≤2,70,-,30,Kabuterimon,6
-,Agunimon,54000,-,4800,3900,-,3900,≤39,≤1,70,-,30,-,7
GoldVeedramon,MegaSeadramon,6000,-,450,450,550,350,40,-,-,-,30,Seadramon,6
-,MetalGreymon,28000,-,3300,2100,-,2100,40,≤1,70,-,30,Greymon,7
-,Rapidmon (Gold),54000,,3600,2700,,3600,≤39,≤1,90,-,,,6
Growlmon (Orange),WarGrowlmon (Orange),1000,2500,600,700,-,-,40,≤2,-,40,30,Growlmon (Orange),6
-,MetalGreymon,28000,-,3300,2100,-,2100,40,≤1,70,-,30,Greymon,7
-,BlackWarGrowlmon,6000,-,900,900,-,-,40,-,70,≤39,30,,6
Greymon (Blue),MetalGreymon (Blue),6000,-,700,500,-,600,40,-,-,≤49,30,Greymon (Blue),6
-,SkullGreymon,3000,3000,900,900,-,-,≤44,5,-,≤39,30,-,6
-,Gigadramon,3000,3000,600,600,-,600,40,-,-,-,30,-,6
Garurumon (Black),WereGarurumon (Black),6000,-,-,550,550,700,≤39,-,-,≤49,30,Garurumon(Black),6
-,Doumon,3500,-,400,,500,400,≤34,,,≤29,30,Youkomon,6
-,Etemon,3500,-,400,400,-,500,≤39,-,-,≤29,30,-,6
RedVegiemon,Lillymon,-,6000,400,-,700,700,≤29,≤2,70,,-,Togemon,6
-,WarGrowlmon (Orange),1000,2500,600,700,-,-,40,≤2,-,40,30,Growlmon (Orange),6
-,MegaKabuterimon,9500,-,800,750,-,750,40,≤2,70,-,30,Kabuterimon,6
IceDevimon,Gigadramon,3000,3000,600,600,-,600,40,-,-,-,30,-,6
-,Zudomon,6000,-,800,500,500,,,≤2,70,-,30,,6
-,Taomon (Silver),-,6000,450,300,600,450,≤34,≤2,,-,-,Kyuubimon (Silver),6
Sorcermon,IceLeomon,28000,-,2600,2600,-,2300,-,≤2,70,60,30,-,6
-,MagnaAngemon,9500,-,650,650,1000,0,≤39,≤1,70,70,0,Angemon,7
-,BlueMeramon,3000,2000,400,-,350,400,≤35,-,-,-,30,Meramon,6
Chrysalimon,Infermon,27000,27000,3300,3000,3000,3300,-,5,-,-,40,-,7
-,Datamon,2000,4000,-,600,700,500,≤24,≤1,-,65,-,-,6
-,MegaKabuterimon (Blue),2000,1500,300,300,400,300,40,-,-,≤49,30,Kabuterimon,6
Numemon,Monzaemon,6000,-,600,700,500,-,40,≤1,70,-,-,-,6
Geremon,Etemon,3500,-,400,400,-,500,≤39,-,-,≤29,30,-,6
Meicoomon,LadyDevimon,-,6000,600,-,600,600,≤39,≤1,-,≤39,-,-,6
-,Angewomon,-,6000,600,-,700,500,≤34,≤1,70,70,-,Gatomon,7
-,Meicrackmon VM,3500,6000,500,500,600,700,≤29,-,70,-,30,-,7`;

const ultimate = `Name,Evolution,HP,MP,Strength,Stamina,Wisdom,Speed,Weight,Mistakes,Bond,Discipline,Wins,Key Digimon,Key Points
MetalGreymon,WarGreymon,16000,-,2100,1700,-,1800,55,≤1,90,75,45,MetalGreymon,8
-,ShineGreymon,6000,10000,1600,1200,1350,1450,50,≤1,90,-,45,-,8
,KaiserGreymon,16000,-,3000,1600,-,1000,≤49,0,90,90,55,-,8
-,Alphamon,24000,24000,5000,4600,4400,-,-,≤1,90,75,55,-,8
WereGarurumon,MetalGarurumon,16000,-,1600,2000,-,2000,55,≤1,90,,45,WereGarurumon,8
-,Jijimon,,8000,1150,,1900,1150,≤44,1,90,75,-,,7
-,MagnaGarurumon,-,16000,-,2000,800,2800,≤49,0,90,90,55,-,8
-,Leopardmon,8000,8000,1250,1050,2000,1300,≤54,0,-,-,55,-,8
Garudamon,Phoenixmon,8000,9000,-,-,1800,1600,≤44,≤1,90,75,-,Garudamon,7
-,Magnadramon,10000,10000,-,-,2600,2600,≤49,0,90,90,45,Angewomon,8
-,Beelzemon,16000,-,1400,1000,1400,1800,-,5,90,≤9,55,-,8
-,Justimon,6000,10000,1900,-,2200,1500,≤34,5,-,≤9,45,-,7
MagnaAngemon,Seraphimon,16000,-,1800,-,2100,1700,≤44,0,90,90,45,MagnaAngemon,8
-,Cherubimon,10000,6000,-,1600,2200,1800,-,0,90,90,-,Antylamon,7
-,Jesmon,20000,28000,3600,3300,3300,3800,≤49,0,90,90,55,-,8
,MarineAngemon,-,4000,-,700,700,1400,≤19,≤1,75,75,-,-,7
MegaKabuterimon,HerculesKabuterimon,10000,-,1300,1700,-,1200,50,-,90,75,45,MegaKabuterimon,
-,ChaosGallantmon,5000,5000,1400,1600,-,1200,-,≤1,90,-,45,BlackWarGrowlmon,7
-,Craniamon,8500,7500,1700,2200,1700,-,60,0,-,90,55,-,8
Zudomon,Vikemon,10000,-,1400,1600,1200,-,60,≤1,90,-,45,Zudomon,7
-,MetalSeadramon,5000,5000,900,1100,1100,1100,60,-,-,-,45,MegaSeadramon,7
-,Titamon,16000,-,2150,2800,-,650,70,≤1,-,≤19,55,-,7
-,Boltmon,10000,-,2000,2200,-,600,60,≤1,75,-,45,-,7
Lillymon,Rosemon,-,10000,-,1100,1700,1400,≤35,≤1,90,75,-,Lillymon,8
-,Sakuyamon,-,10000,1100,-,1700,1400,≤39,≤1,90,75,-,Taomon,7
-,Lilithmon,20000,28000,4600,-,5800,3600,≤34,5,-,≤9,45,-,8
-,Minervamon,40000,8000,5000,4000,2000,3000,≤49,0,90,90,55,-,8
Angewomon,Magnadramon,10000,10000,-,-,2600,2600,≤49,0,90,90,45,Angewomon,8
-,Ophanimon,8000,2000,-,1500,1400,1300,≤49,0,90,90,-,-,8
-,Crusadermon,10000,6000,1300,1300,1400,1600,-,0,-,90,45,-,8
,MarineAngemon,-,4000,-,700,700,1400,≤19,≤1,75,75,-,-,7
Paildramon,Magnamon,16000,-,1200,1800,1200,1400,-,0,90,75,45,-,8
-,Imperialdramon DM,24000,24000,5600,4600,-,3800,65,-,90,-,55,Paildramon,8
-,UlforceVeedramon,48000,-,4200,-,3800,6000,≤49,≤1,90,75,45,AeroVeedramon,8
WarGrowlmon,Gallantmon,16000,-,1900,2500,1200,-,50,≤1,90,75,45,WarGrowlmon,8
-,ShineGreymon,6000,10000,1600,1200,1350,1450,50,≤1,90,-,45,-,8
-,Leviamon,10000,6000,1900,2300,-,1400,65,5,-,≤9,55,-,8
-,KaiserGreymon,16000,-,3000,1600,-,1000,≤49,0,90,90,55,-,8
Rapidmon,MegaGargomon,10000,-,1200,1700,1300,-,60,-,90,75,45,Rapidmon,7
-,Vikemon,10000,-,1400,1600,1200,-,60,≤1,90,-,45,Zudomon,7
-,Kentaurosmon,16000,-,1200,1300,1300,1700,≤54,-,90,75,45,-,8
-,MagnaGarurumon,-,16000,-,2000,800,2800,≤49,0,90,90,55,-,8
Antylamon,Cherubimon,10000,6000,-,1600,2200,1800,-,0,90,90,-,Antylamon,7
-,Ophanimon,8000,2000,-,1500,1400,1300,≤49,0,90,90,-,-,8
-,Belphemon SM,20000,28000,3600,5800,4600,-,65,5,-,≤9,45,-,8
Taomon,Sakuyamon,-,10000,1100,-,1700,1400,≤39,≤1,90,75,-,Taomon,7
-,Dianamon,8000,40000,3000,2000,4000,5000,≤49,0,90,90,55,-,8
-,Arresterdramon,,,,,,1000,,,,,,,
-,Barbamon,8000,,1900,1400,2300,-,,5,,≤9,45,-,
MachGaogamon,MirageGaogamon,10000,-,1100,900,900,1300,≤49,-,90,-,45,MachGaogamon,7
-,MetalGarurumon,16000,-,1600,2000,-,2000,55,≤1,90,,45,WereGarurumon,8
-,Gankoomon,8000,8000,1400,1400,1400,1400,-,≤1,90,75,55,-,8
-,Justimon,6000,10000,1900,-,2200,1500,≤34,5,-,≤9,45,-,7
AeroVeedramon,UlforceVeedramon,48000,-,4200,-,3800,6000,≤49,≤1,90,75,45,AeroVeedramon,8
-,OmniShoutmon,20000,11400,2500,1100,1000,2400,≤49,0,90,99,55,-,
-,Jesmon,20000,28000,3600,3300,3300,3800,≤49,0,90,90,55,-,8
,DarkDramon,10000,-,2200,-,2000,-,≤49,0,90,90,55,-,8
Myotismon,VenomMyotismon,10000,-,1000,500,1200,1500,60,-,-,≤19,45,Myotismon,7
-,Jijimon,,8000,1150,,1900,1150,≤44,1,90,75,-,,7
-,Barbamon,8000,,1900,1400,2300,-,,5,,≤9,45,-,
-,Piedmon,4000,6000,900,-,1800,1500,≤34,5,-,≤9,45,-,7
LadyDevimon,Lilithmon,20000,28000,4600,-,5800,3600,≤34,5,-,≤9,45,-,8
-,Rosemon,-,10000,-,1100,1700,1400,≤35,≤1,90,75,-,Lillymon,8
-,Kuzuhamon,-,10000,1300,-,1600,1300,≤39,0,-,≤19,-,Doumon,7
-,Piedmon,4000,6000,900,-,1800,1500,≤34,5,-,≤9,45,-,7
Mamemon,Kentaurosmon,16000,-,1200,1300,1300,1700,≤54,-,90,75,45,-,8
-,PlatinumNumemon,4000,-,500,,700,,60,,75,,-,,7
-,Justimon,6000,10000,1900,-,2200,1500,≤34,5,-,≤9,45,-,7
-,Jijimon,,8000,1150,,1900,1150,≤44,1,90,75,-,,7
MetalMamemon,Dynasmon,8000,8000,1400,1400,1400,1400,-,≤1,90,-,45,-,8
-,Machinedramon,28000,20000,5000,3800,2600,2600,85,-,-,-,55,-,7
-,MetalGarurumon,16000,-,1600,2000,-,2000,55,≤1,90,,45,WereGarurumon,8
-,MagnaGarurumon,-,16000,-,2000,800,2800,≤49,0,90,90,55,-,8
MetalTyrannomon,WarGreymon,16000,-,2100,1700,-,1800,55,≤1,90,75,45,MetalGreymon,8
-,MetalSeadramon,5000,5000,900,1100,1100,1100,60,-,-,-,45,MegaSeadramon,7
-,RustTyranomon,16000,-,2100,1900,-,1600,55,-,90,-,45,MetalTyrannomon,7
SkullGreymon,Craniamon,8500,7500,1700,2200,1700,-,60,0,-,90,55,-,8
-,Titamon,16000,-,2150,2800,-,650,70,≤1,-,≤19,55,-,7
,BlackWarGreymon,10000,-,1200,1100,900,1000,-,-,-,≤24,45,MetalGreymon (Blue),7
Gigadramon,Machinedramon,28000,20000,5000,3800,2600,2600,85,-,-,-,55,-,7
-,RustTyranomon,16000,-,2100,1900,-,1600,55,-,90,-,45,MetalTyrannomon,7
-,Justimon,6000,10000,1900,-,2200,1500,≤34,5,-,≤9,45,-,7
-,Dynasmon,8000,8000,1400,1400,1400,1400,-,≤1,90,-,45,-,8
Megadramon,Samudramon,,,2200,1200,-,800,≤49,-,-,≤29,,-,
-,WarGreymon,16000,-,2100,1700,-,1800,55,≤1,90,75,45,MetalGreymon,8
-,DarkDramon,10000,-,2200,-,2000,-,≤49,0,90,90,55,-,8
-,Belphemon SM,20000,28000,3600,5800,4600,-,65,5,-,≤9,45,-,8
IceLeomon,Gankoomon,8000,8000,1400,1400,1400,1400,-,≤1,90,75,55,-,8
-,Magnadramon,10000,10000,-,-,2600,2600,≤49,0,90,90,45,Angewomon,8
GrapLeomon,BanchoLeomon,10000,-,1250,1050,1050,850,-,-,90,60,55,GrapLeomon,
-,MegaGargomon,10000,-,1200,1700,1300,-,60,-,90,75,45,Rapidmon,7
-,Dynasmon,8000,8000,1400,1400,1400,1400,-,≤1,90,-,45,-,8
BlueMeramon,Leviamon,10000,6000,1900,2300,-,1400,65,5,-,≤9,55,-,8
-,MirageGaogamon,10000,-,1100,900,900,1300,≤49,-,90,-,45,MachGaogamon,7
-,Boltmon,10000,-,2000,2200,-,600,60,≤1,75,-,45,-,7
-,Gankoomon,8000,8000,1400,1400,1400,1400,-,≤1,90,75,55,-,8
MegaSeadramon,MetalSeadramon,5000,5000,900,1100,1100,1100,60,-,-,-,45,MegaSeadramon,7
-,Machinedramon,28000,20000,5000,3800,2600,2600,85,-,-,-,55,-,7
-,MarineAngemon,-,4000,-,700,700,1400,≤19,≤1,75,75,-,-,7
-,Leviamon,10000,6000,1900,2300,-,1400,65,5,-,≤9,55,-,8
Okuwamon,GranKuwagamon,7000,3000,1600,1400,-,1200,55,-,-,≤19,45,Okuwamon,7
-,Examon,8000,8000,2100,1000,1000,1500,70,-,75,-,55,-,8
-,Diaboromon,30000,30000,4500,4500,6000,6000,-,10,-,-,55,-,8
Datamon,Barbamon,8000,,1900,1400,2300,-,,5,,≤9,45,-,
-,PlatinumNumemon,4000,-,500,,700,,60,,75,,-,,7
-,MetalGarurumon (Black),10000,-,1400,1400,-,1400,-,≤1,75,≤24,-,WereGarurumon (Black),7
Meteormon,Belphemon SM,20000,28000,3600,5800,4600,-,65,5,-,≤9,45,-,8
-,Dianamon,8000,40000,3000,2000,4000,5000,≤49,0,90,90,55,-,8
-,MetalEtemon,6000,4000,1400,1400,-,1400,60,-,-,≤19,45,Etemon,7
-,Cherubimon,10000,6000,-,1600,2200,1800,-,0,90,90,-,Antylamon,7
MetalGreymon (Blue),BlackWarGreymon,10000,-,1200,1100,900,1000,-,-,-,≤24,45,MetalGreymon (Blue),7
-,Samudramon,,,2200,1200,-,800,≤49,-,-,≤29,,-,
-,Alphamon,24000,24000,5000,4600,4400,-,-,≤1,90,75,55,-,8
-,Boltmon,10000,-,2000,2200,-,600,60,≤1,75,-,45,-,7
WereGarurumon (Black),MetalGarurumon (Black),10000,-,1400,1400,-,1400,-,≤1,75,≤24,-,WereGarurumon (Black),7
-,Leopardmon,8000,8000,1250,1050,2000,1300,≤54,0,-,-,55,-,8
,Minervamon,40000,8000,5000,4000,2000,3000,≤49,0,90,90,55,-,8
,MirageGaogamon,10000,-,1100,900,900,1300,≤49,-,90,-,45,MachGaogamon,7
MegaKabuterimon (Blue),Craniamon,8500,7500,1700,2200,1700,-,60,0,-,90,55,-,8
-,GranKuwagamon,7000,3000,1600,1400,-,1200,55,-,-,≤19,45,Okuwamon,7
,Kentaurosmon,16000,-,1200,1300,1300,1700,≤54,-,90,75,45,-,8
WarGrowlmon (Orange),Examon,8000,8000,2100,1000,1000,1500,70,-,75,-,55,-,8
-,RustTyranomon,16000,-,2100,1900,-,1600,55,-,90,-,45,MetalTyrannomon,7
-,WarGreymon,16000,-,2100,1700,-,1800,55,≤1,90,75,45,MetalGreymon,8
BlackWarGrowlmon,ChaosGallantmon,5000,5000,1400,1600,-,1200,-,≤1,90,-,45,BlackWarGrowlmon,7
-,Beelzemon,16000,-,1400,1000,1400,1800,-,5,90,≤9,55,-,8
-,DarkDramon,10000,-,2200,-,2000,-,≤49,0,90,90,55,-,8
Rapidmon (Gold),Leopardmon,8000,8000,1250,1050,2000,1300,≤54,0,-,-,55,-,8
-,HerculesKabuterimon,10000,-,1300,1700,-,1200,50,-,90,75,45,MegaKabuterimon,
-,OmniShoutmon,20000,11400,2500,1100,1000,2400,≤49,0,90,99,55,-,
Taomon (Silver),Crusadermon,10000,6000,1300,1300,1400,1600,-,0,-,90,45,-,8
-,Phoenixmon,8000,9000,-,-,1800,1600,≤44,≤1,90,75,-,Garudamon,7
-,Dianamon,8000,40000,3000,2000,4000,5000,≤49,0,90,90,55,-,8
-,Kuzuhamon,-,10000,1300,-,1600,1300,≤39,0,-,≤19,-,Doumon,7
Doumon,Kuzuhamon,-,10000,1300,-,1600,1300,≤39,0,-,≤19,-,Doumon,7
-,MetalGarurumon (Black),10000,-,1400,1400,-,1400,-,≤1,75,≤24,-,WereGarurumon (Black),7
-,Lilithmon,20000,28000,4600,-,5800,3600,≤34,5,-,≤9,45,-,8
-,Piedmon,4000,6000,900,-,1800,1500,≤34,5,-,≤9,45,-,7
WaruSeadramon,Creepymon,24000,24000,3500,3500,3500,3500,50,5,-,-,45,-,8
-,MetalEtemon,6000,4000,1400,1400,-,1400,60,-,-,≤19,45,Etemon,7
-,MetalSeadramon,5000,5000,900,1100,1100,1100,60,-,-,-,45,MegaSeadramon,7
Infermon,Diaboromon,30000,30000,4500,4500,6000,6000,-,10,-,-,55,-,8
-,Creepymon,24000,24000,3500,3500,3500,3500,50,5,-,-,45,-,8
-,VenomMyotismon,10000,-,1000,500,1200,1500,60,-,-,≤19,45,Myotismon,7
Etemon,MetalEtemon,6000,4000,1400,1400,-,1400,60,-,-,≤19,45,Etemon,7
-,MegaGargomon,10000,-,1200,1700,1300,-,60,-,90,75,45,Rapidmon,7
-,PlatinumNumemon,4000,-,500,950,700,650,60,-,75,75,-,-,7
Monzaemon,PlatinumNumemon,4000,-,500,950,700,650,60,-,75,75,-,-,7
-,Seraphimon,16000,-,1800,-,2100,1700,≤44,0,90,90,45,MagnaAngemon,8
-,Minervamon,40000,8000,5000,4000,2000,3000,≤49,0,90,90,55,-,8
-,Vikemon,10000,-,1400,1600,1200,-,60,≤1,90,-,45,Zudomon,7
Agunimon,KaiserGreymon,16000,-,3000,1600,-,1000,≤49,0,90,90,55,-,8
-,Gallantmon,16000,-,1900,2500,1200,-,50,≤1,90,75,45,WarGrowlmon,8
-,Phoenixmon,8000,9000,-,-,1800,1600,≤44,≤1,90,75,-,Garudamon,7
-,ShineGreymon,6000,10000,1600,1200,1350,1450,50,≤1,90,-,45,-,8
Lucemon FM,VenomMyotismon,10000,-,1000,500,1200,1500,60,-,-,≤19,45,Myotismon,7
-,Lucemon SM,37500,37500,8100,8100,7800,-,-,5,-,≤9,55,Lucemon FM,8
-,Seraphimon,16000,-,1800,-,2100,1700,≤44,0,90,90,45,MagnaAngemon,8
Meicrackmon VM,Mastemon,6000,10000,1300,1300,1600,1400,≤44,-,90,-,-,-,7
-,Ophanimon,8000,2000,-,1500,1400,1300,≤49,0,90,90,-,-,8
-,Rosemon,-,10000,-,1100,1700,1400,≤35,≤1,90,75,-,Lillymon,8`;

module.exports = { baby, inTraining, rookie, champion, ultimate };
